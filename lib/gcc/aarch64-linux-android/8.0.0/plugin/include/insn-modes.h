/* Generated automatically from machmode.def and config/aarch64/aarch64-modes.def
   by genmodes.  */

#ifndef GCC_INSN_MODES_H
#define GCC_INSN_MODES_H

enum machine_mode
{
  VOIDmode,                /* machmode.def:172 */
#define HAVE_VOIDmode
  BLKmode,                 /* machmode.def:176 */
#define HAVE_BLKmode
  CCmode,                  /* machmode.def:214 */
#define HAVE_CCmode
  CCFPmode,                /* config/aarch64/aarch64-modes.def:21 */
#define HAVE_CCFPmode
  CCFPEmode,               /* config/aarch64/aarch64-modes.def:22 */
#define HAVE_CCFPEmode
  CC_SWPmode,              /* config/aarch64/aarch64-modes.def:23 */
#define HAVE_CC_SWPmode
  CC_NZmode,               /* config/aarch64/aarch64-modes.def:24 */
#define HAVE_CC_NZmode
  CC_Zmode,                /* config/aarch64/aarch64-modes.def:25 */
#define HAVE_CC_Zmode
  CC_Cmode,                /* config/aarch64/aarch64-modes.def:26 */
#define HAVE_CC_Cmode
  BImode,                  /* machmode.def:179 */
#define HAVE_BImode
  QImode,                  /* machmode.def:187 */
#define HAVE_QImode
  HImode,                  /* machmode.def:188 */
#define HAVE_HImode
  SImode,                  /* machmode.def:189 */
#define HAVE_SImode
  DImode,                  /* machmode.def:190 */
#define HAVE_DImode
  TImode,                  /* machmode.def:191 */
#define HAVE_TImode
  OImode,                  /* config/aarch64/aarch64-modes.def:40 */
#define HAVE_OImode
  CImode,                  /* config/aarch64/aarch64-modes.def:44 */
#define HAVE_CImode
  XImode,                  /* config/aarch64/aarch64-modes.def:45 */
#define HAVE_XImode
  QQmode,                  /* machmode.def:217 */
#define HAVE_QQmode
  HQmode,                  /* machmode.def:218 */
#define HAVE_HQmode
  SQmode,                  /* machmode.def:219 */
#define HAVE_SQmode
  DQmode,                  /* machmode.def:220 */
#define HAVE_DQmode
  TQmode,                  /* machmode.def:221 */
#define HAVE_TQmode
  UQQmode,                 /* machmode.def:223 */
#define HAVE_UQQmode
  UHQmode,                 /* machmode.def:224 */
#define HAVE_UHQmode
  USQmode,                 /* machmode.def:225 */
#define HAVE_USQmode
  UDQmode,                 /* machmode.def:226 */
#define HAVE_UDQmode
  UTQmode,                 /* machmode.def:227 */
#define HAVE_UTQmode
  HAmode,                  /* machmode.def:229 */
#define HAVE_HAmode
  SAmode,                  /* machmode.def:230 */
#define HAVE_SAmode
  DAmode,                  /* machmode.def:231 */
#define HAVE_DAmode
  TAmode,                  /* machmode.def:232 */
#define HAVE_TAmode
  UHAmode,                 /* machmode.def:234 */
#define HAVE_UHAmode
  USAmode,                 /* machmode.def:235 */
#define HAVE_USAmode
  UDAmode,                 /* machmode.def:236 */
#define HAVE_UDAmode
  UTAmode,                 /* machmode.def:237 */
#define HAVE_UTAmode
  HFmode,                  /* config/aarch64/aarch64-modes.def:29 */
#define HAVE_HFmode
  SFmode,                  /* machmode.def:209 */
#define HAVE_SFmode
  DFmode,                  /* machmode.def:210 */
#define HAVE_DFmode
  TFmode,                  /* config/aarch64/aarch64-modes.def:58 */
#define HAVE_TFmode
  SDmode,                  /* machmode.def:249 */
#define HAVE_SDmode
  DDmode,                  /* machmode.def:250 */
#define HAVE_DDmode
  TDmode,                  /* machmode.def:251 */
#define HAVE_TDmode
  CQImode,                 /* machmode.def:245 */
#define HAVE_CQImode
  CHImode,                 /* machmode.def:245 */
#define HAVE_CHImode
  CSImode,                 /* machmode.def:245 */
#define HAVE_CSImode
  CDImode,                 /* machmode.def:245 */
#define HAVE_CDImode
  CTImode,                 /* machmode.def:245 */
#define HAVE_CTImode
  COImode,                 /* machmode.def:245 */
#define HAVE_COImode
  CCImode,                 /* machmode.def:245 */
#define HAVE_CCImode
  CXImode,                 /* machmode.def:245 */
#define HAVE_CXImode
  HCmode,                  /* machmode.def:246 */
#define HAVE_HCmode
  SCmode,                  /* machmode.def:246 */
#define HAVE_SCmode
  DCmode,                  /* machmode.def:246 */
#define HAVE_DCmode
  TCmode,                  /* machmode.def:246 */
#define HAVE_TCmode
  V8QImode,                /* config/aarch64/aarch64-modes.def:33 */
#define HAVE_V8QImode
  V4HImode,                /* config/aarch64/aarch64-modes.def:33 */
#define HAVE_V4HImode
  V2SImode,                /* config/aarch64/aarch64-modes.def:33 */
#define HAVE_V2SImode
  V16QImode,               /* config/aarch64/aarch64-modes.def:34 */
#define HAVE_V16QImode
  V8HImode,                /* config/aarch64/aarch64-modes.def:34 */
#define HAVE_V8HImode
  V4SImode,                /* config/aarch64/aarch64-modes.def:34 */
#define HAVE_V4SImode
  V2DImode,                /* config/aarch64/aarch64-modes.def:34 */
#define HAVE_V2DImode
  V32QImode,               /* config/aarch64/aarch64-modes.def:48 */
#define HAVE_V32QImode
  V16HImode,               /* config/aarch64/aarch64-modes.def:48 */
#define HAVE_V16HImode
  V8SImode,                /* config/aarch64/aarch64-modes.def:48 */
#define HAVE_V8SImode
  V4DImode,                /* config/aarch64/aarch64-modes.def:48 */
#define HAVE_V4DImode
  V2TImode,                /* config/aarch64/aarch64-modes.def:48 */
#define HAVE_V2TImode
  V48QImode,               /* config/aarch64/aarch64-modes.def:51 */
#define HAVE_V48QImode
  V24HImode,               /* config/aarch64/aarch64-modes.def:51 */
#define HAVE_V24HImode
  V12SImode,               /* config/aarch64/aarch64-modes.def:51 */
#define HAVE_V12SImode
  V6DImode,                /* config/aarch64/aarch64-modes.def:51 */
#define HAVE_V6DImode
  V3TImode,                /* config/aarch64/aarch64-modes.def:51 */
#define HAVE_V3TImode
  V64QImode,               /* config/aarch64/aarch64-modes.def:54 */
#define HAVE_V64QImode
  V32HImode,               /* config/aarch64/aarch64-modes.def:54 */
#define HAVE_V32HImode
  V16SImode,               /* config/aarch64/aarch64-modes.def:54 */
#define HAVE_V16SImode
  V8DImode,                /* config/aarch64/aarch64-modes.def:54 */
#define HAVE_V8DImode
  V4TImode,                /* config/aarch64/aarch64-modes.def:54 */
#define HAVE_V4TImode
  V2OImode,                /* config/aarch64/aarch64-modes.def:54 */
#define HAVE_V2OImode
  V4HFmode,                /* config/aarch64/aarch64-modes.def:35 */
#define HAVE_V4HFmode
  V2SFmode,                /* config/aarch64/aarch64-modes.def:35 */
#define HAVE_V2SFmode
  V1DFmode,                /* config/aarch64/aarch64-modes.def:37 */
#define HAVE_V1DFmode
  V8HFmode,                /* config/aarch64/aarch64-modes.def:36 */
#define HAVE_V8HFmode
  V4SFmode,                /* config/aarch64/aarch64-modes.def:36 */
#define HAVE_V4SFmode
  V2DFmode,                /* config/aarch64/aarch64-modes.def:36 */
#define HAVE_V2DFmode
  V16HFmode,               /* config/aarch64/aarch64-modes.def:49 */
#define HAVE_V16HFmode
  V8SFmode,                /* config/aarch64/aarch64-modes.def:49 */
#define HAVE_V8SFmode
  V4DFmode,                /* config/aarch64/aarch64-modes.def:49 */
#define HAVE_V4DFmode
  V24HFmode,               /* config/aarch64/aarch64-modes.def:52 */
#define HAVE_V24HFmode
  V12SFmode,               /* config/aarch64/aarch64-modes.def:52 */
#define HAVE_V12SFmode
  V6DFmode,                /* config/aarch64/aarch64-modes.def:52 */
#define HAVE_V6DFmode
  V32HFmode,               /* config/aarch64/aarch64-modes.def:55 */
#define HAVE_V32HFmode
  V16SFmode,               /* config/aarch64/aarch64-modes.def:55 */
#define HAVE_V16SFmode
  V8DFmode,                /* config/aarch64/aarch64-modes.def:55 */
#define HAVE_V8DFmode
  MAX_MACHINE_MODE,

  MIN_MODE_RANDOM = VOIDmode,
  MAX_MODE_RANDOM = BLKmode,

  MIN_MODE_CC = CCmode,
  MAX_MODE_CC = CC_Cmode,

  MIN_MODE_INT = QImode,
  MAX_MODE_INT = XImode,

  MIN_MODE_PARTIAL_INT = VOIDmode,
  MAX_MODE_PARTIAL_INT = VOIDmode,

  MIN_MODE_POINTER_BOUNDS = VOIDmode,
  MAX_MODE_POINTER_BOUNDS = VOIDmode,

  MIN_MODE_FRACT = QQmode,
  MAX_MODE_FRACT = TQmode,

  MIN_MODE_UFRACT = UQQmode,
  MAX_MODE_UFRACT = UTQmode,

  MIN_MODE_ACCUM = HAmode,
  MAX_MODE_ACCUM = TAmode,

  MIN_MODE_UACCUM = UHAmode,
  MAX_MODE_UACCUM = UTAmode,

  MIN_MODE_FLOAT = HFmode,
  MAX_MODE_FLOAT = TFmode,

  MIN_MODE_DECIMAL_FLOAT = SDmode,
  MAX_MODE_DECIMAL_FLOAT = TDmode,

  MIN_MODE_COMPLEX_INT = CQImode,
  MAX_MODE_COMPLEX_INT = CXImode,

  MIN_MODE_COMPLEX_FLOAT = HCmode,
  MAX_MODE_COMPLEX_FLOAT = TCmode,

  MIN_MODE_VECTOR_INT = V8QImode,
  MAX_MODE_VECTOR_INT = V2OImode,

  MIN_MODE_VECTOR_FRACT = VOIDmode,
  MAX_MODE_VECTOR_FRACT = VOIDmode,

  MIN_MODE_VECTOR_UFRACT = VOIDmode,
  MAX_MODE_VECTOR_UFRACT = VOIDmode,

  MIN_MODE_VECTOR_ACCUM = VOIDmode,
  MAX_MODE_VECTOR_ACCUM = VOIDmode,

  MIN_MODE_VECTOR_UACCUM = VOIDmode,
  MAX_MODE_VECTOR_UACCUM = VOIDmode,

  MIN_MODE_VECTOR_FLOAT = V4HFmode,
  MAX_MODE_VECTOR_FLOAT = V8DFmode,

  NUM_MACHINE_MODES = MAX_MACHINE_MODE
};

#define CONST_MODE_SIZE const
#define CONST_MODE_UNIT_SIZE const
#define CONST_MODE_BASE_ALIGN const
#define CONST_MODE_IBIT const
#define CONST_MODE_FBIT const

#define BITS_PER_UNIT (8)
#define MAX_BITSIZE_MODE_ANY_INT (64*BITS_PER_UNIT)
#define MAX_BITSIZE_MODE_ANY_MODE (128*BITS_PER_UNIT)
#define NUM_INT_N_ENTS 1

#endif /* insn-modes.h */
